import React, { useState } from 'react'
import { Card, Button, Form, Spinner, Alert} from 'react-bootstrap'

const SectionOne = (({nextSection}) => {
  const [userAnswers, setUserAnswers] =  useState({q1 : '', q2: '', q3: ''})
  return (
    <section className='section'>
      <Card>
      <Card.Header>Section One</Card.Header>
      <Card.Body>
          <Card.Title>1. Is your business model B2C or B2B or both?</Card.Title>
          <div>
            <Form.Check
              type='radio'
              name='qustionOne'
              value='B2C'
              onChange={(event) => setUserAnswers({...userAnswers, q1: event.target.value})}
              label='A. B2C'
            />
            <Form.Check
              type='radio'
              name='qustionOne'
              value='B2B'
              onChange={(event) => setUserAnswers({...userAnswers, q1: event.target.value})}
              label='B. B2B'
            />
            <Form.Check
              type='radio'
              name='qustionOne'
              value='both'
              onChange={(event) => setUserAnswers({...userAnswers, q1: event.target.value})}
              label='C. both'
            />
          </div>
      </Card.Body>
      {(userAnswers.q1 === 'B2B' || userAnswers.q1 === 'both') &&
        <Card.Body>
          <Card.Title>2. Do you target all age brackets?</Card.Title>
          <div>
            <Form.Check
              type='radio'
              name='qustionTwo'
              value='yes'
              onChange={(event) => setUserAnswers({...userAnswers, q2: event.target.value})}
              label='A. yes'
            />
            <Form.Check
              type='radio'
              name='qustionTwo'
              value='no'
              onChange={(event) => setUserAnswers({...userAnswers, q2: event.target.value})}
              label='B. no'
            />
          </div>
      </Card.Body> }
      {(userAnswers.q1 === 'B2C' || userAnswers.q1 === 'both') &&
        <Card.Body>
          <Card.Title>3. Do you target all industries?</Card.Title>
          <div>
            <Form.Check
              type='radio'
              name='qustionThree'
              value='yes'
              onChange={(event) => setUserAnswers({...userAnswers, q3: event.target.value})}
              label='A. yes'
            />
            <Form.Check
              type='radio'
              name='qustionThree'
              value='no'
              onChange={(event) => setUserAnswers({...userAnswers, q3: event.target.value})}
              label='B. no'
            />
          </div>
      </Card.Body> }
      <Card.Footer>
        <Button variant="primary" onClick={() => nextSection('sectionTwo', userAnswers)}>Next</Button>
      </Card.Footer>
    </Card>
    </section>
  )
})
const SectionTwo = (({nextSection}) => {
  const [userAnswers, setUserAnswers] =  useState({q4 : '', q5: 0})
  return (
    <section className='section'>
      <Card>
      <Card.Header>Section Two</Card.Header>
      <Card.Body>
          <Card.Title>1. Did you have an investment?</Card.Title>
          <div>
            <Form.Check
              type='radio'
              name='qustionOne'
              value='yes'
              onChange={(event) => setUserAnswers({...userAnswers, q4: event.target.value})}
              label='A. yes'
            />
            <Form.Check
              type='radio'
              name='qustionOne'
              value='no'
              onChange={(event) => setUserAnswers({...userAnswers, q4: event.target.value})}
              label='B. no'
            />
          </div>
      </Card.Body>
        <Card.Body>
          <Card.Title>2. how much was the investment?</Card.Title>
          <div>
          <Form.Control
              type="number"
              min="0"
              onChange={(event) => setUserAnswers({...userAnswers, q5: event.target.value})}
              disabled={userAnswers.q4 === 'yes'? false : true}
            />
          </div>
      </Card.Body>       
      <Card.Footer>
        <Button variant="primary" onClick={() => nextSection('submit', userAnswers)}>submit</Button>
      </Card.Footer>
    </Card>
    </section>
  )
})

function Questionnaire() {
  const [currentSection, setCurrentSection] = useState('sectionOne')
  const [allUserAnswers, setAllUserAnswers] = useState({})
  const nextSection = (type, answers) => {
    setCurrentSection(type)
    setAllUserAnswers({...allUserAnswers, ...answers})
    if(type === 'submit'){
      submitDataToApi(allUserAnswers)
      setTimeout(() => {
        setCurrentSection('submitted')
      }, 2000);
    }
  }
  // Submit Data To API
  const submitDataToApi = (answers) => {
    // axios.post('/testapi', answers).then(res => {
    //   console.log('res :>> ', res);
    // }).catch(err => {
    //   console.log('error :>> ', error);
    // })
  }

  return (
    <main className='p-5'>
      { currentSection === 'sectionOne' && <SectionOne nextSection={nextSection}/>}
      { currentSection === 'sectionTwo' && <SectionTwo nextSection={nextSection}/>}
      { currentSection === 'submit' && <div className="d-flex align-items-center justify-content-center py-5 my-5">
        <Spinner animation="border" role="status"><span className="visually-hidden">Loading...</span> </Spinner>
      </div>}
      { currentSection === 'submitted' && <div className="d-flex align-items-center justify-content-center py-5 my-5">
          <Alert variant='success'>your answers are submitted successfully</Alert>
      </div>}
    </main>
  )
}

export default Questionnaire