import React from 'react'
import { Button } from 'react-bootstrap'
import { Link } from 'react-router-dom'

function Home() {
  return (
    <main className='p-5 my-5 d-flex align-items-center justify-content-center'>
      <Link to="/questionnaire">
        <Button variant="primary" size="lg">Generate Business Plan</Button>
      </Link>
    </main>
  )
}

export default Home