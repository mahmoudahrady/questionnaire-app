
import * as React from "react";
import { Routes, Route } from "react-router-dom";
import "./App.css";
import Home from './components/Home';
import Questionnaire from './components/Questionnaire';
import { Container } from 'react-bootstrap';
function App() {
  return (
    <div className="App">
      <Container>
        <Routes>
          <Route path="/" element={<Home />} />
          <Route path="/questionnaire" element={<Questionnaire />} />
        </Routes>
      </Container>
    </div>
  );
}
export default App;